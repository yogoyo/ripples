#include <complex.h>
#include <math.h>
#include <tgmath.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include "aalib.h"
#ifdef __MACH__
#include <mach/clock.h>
#include <mach/mach_time.h>
#include <mach/mach.h>
#endif


static inline double clip(double value, double low, double high) {
	return fmin(fmax((value - low) / (high - low), 0), 1);
}

#define NS_PER_SEC 1000000000ll

struct ripple {
	double x;
	double y;
	double phase;
	double amp;
	double freq;
	double vel;
	double friction;
	double decay;
	double time;
};

double spatial_ripple_value(double r, struct ripple *ripple) {
	double dt = ripple->time - r / ripple->vel;
	if (dt < 0) return 0;
	return exp(-ripple->decay * dt -ripple->friction*r) * ripple->amp * sin(ripple->phase + ripple->freq * dt);
}

double drand(void) {
	int64_t r = (int64_t)rand() * RAND_MAX + rand();
	return ((double)r) / pow((double)RAND_MAX, 2);
}

double rand_uniform(double low, double high) {
	return drand() * (high - low) + low;
}

#define TIME_ADD_NS(tm, ns) do { long _tmp = tm.tv_nsec + ns; tm.tv_nsec = _tmp % NS_PER_SEC; tm.tv_sec += _tmp / NS_PER_SEC; } while(0)
#define TIME_SUB_NS(t1, t2) ((t1).tv_nsec - (t2).tv_nsec + ((t1).tv_sec - (t2).tv_sec) * NS_PER_SEC)


int ripples_tick(struct ripple *ripples, int ripple_count, int max_ripples, double tick_interval, double w, double h)
{
	// ripple properties
	const double friction = 3e-3;
	const double velocity = 60;  /* propagation speed */

	int dying = 0;
#define AMP_CUTOFF 1e-2
	// decay ripples and update times
	for (int i = 0; i < ripple_count; i++) {
		//ripples[i].amp *= ripples[i].decay;
		// this is done to bias for younger ripples
		double value = exp(-ripples[i].decay*ripples[i].time);
		if (value <= 0.2) {
			dying += 1;
		}
		ripples[i].time += tick_interval;
		if (value < AMP_CUTOFF) {
			// cleanup old ripple
			for (int j = i; j < ripple_count; j++) {
				ripples[i] = ripples[j];
			}
			dying -= 1;
			ripple_count--;
		}
	}

#define WAVE_GEN_PROB 0.01
	if (ripple_count < max_ripples && (ripple_count == dying || drand() < WAVE_GEN_PROB)) {
		// spawn a ripple
		ripples[ripple_count].amp = rand_uniform(0.6, 0.8);
		ripples[ripple_count].freq = rand_uniform(3,11);
		ripples[ripple_count].phase = 0;
		ripples[ripple_count].friction = friction;
		ripples[ripple_count].decay = rand_uniform(0.5, 0.8);
		ripples[ripple_count].vel = velocity;
		ripples[ripple_count].time = 0;
		ripples[ripple_count].x = rand_uniform(0, w);
		ripples[ripple_count].y = rand_uniform(0, h);
		ripple_count++;
	}

	return ripple_count;
}


void draw_ripples(aa_context *ctx, struct ripple *ripples, int ripple_count) {
	// TODO render only 1/8th, use symmetries
	for (int x = 0; x < aa_imgwidth(ctx); x++) {
		for (int y = 0; y < aa_imgheight(ctx); y++) {
			double value = 0;
			for (int w = 0; w < ripple_count; w++) {
				double r = hypot(x-ripples[w].x, y*2-ripples[w].y*2);
				value += spatial_ripple_value(r, &ripples[w]);
			}
			value = pow(clip(fabs(value), 0.0, 1), 1);
			aa_putpixel(ctx, x, y, (int)(value * 255));
		}
	}
	// render
	aa_fastrender(ctx, 0, 0, aa_scrwidth(ctx), aa_scrheight(ctx));
//#define DEBUG
#ifdef DEBUG
		for (int w = 0; w < ripple_count; w++) {
			aa_printf(ctx, 1, w+1, AA_NORMAL, "w%d: amp=%lf", w, ripples[w].amp);
		}
#endif
		

}

void sync_vbuf(aa_context *ctx, struct timespec *next_tick) {
#ifdef __MACH__
		struct timespec sleeptime = *next_tick;
		struct timespec now;
		do {
		clock_gettime(CLOCK_MONOTONIC, &now);
		int64_t delta = TIME_SUB_NS(*next_tick, now);
		if (delta < 0) {
			break;
		}
		sleeptime.tv_sec = delta / NS_PER_SEC;
		sleeptime.tv_nsec = delta % NS_PER_SEC;
		} while(nanosleep(&sleeptime, NULL));
#else
		while (clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, next_tick, NULL));
#endif
		aa_flush(ctx);
		
}

aa_context *init_aa_ctx(void) {
	aa_context *context;
	aa_recommendhidisplay("curses");
	aa_recommendhikbd("curses");
	context = aa_autoinit(&aa_defparams);
	aa_autoinitkbd(context, 0);
	if (context == NULL) {
		fprintf(stderr,"Cannot initialize AA-lib. Sorry\n");
		exit(1);
	}
	return context;
}

static void run_loop(void) {
	struct timespec next_tick = { .tv_sec = 0, .tv_nsec = 0 };
	const double fps = 24.0;
	const int64_t tick_ns = (int64_t)(1000000000ul / fps);
	const double tick_interval = 1/fps;


	const int max_ripples = 5;
	struct ripple ripples[max_ripples];
	int ripple_count = 0;
	aa_context *ctx = init_aa_ctx();

	clock_gettime(CLOCK_MONOTONIC, &next_tick);
	srand(next_tick.tv_nsec);


	while (1) {
		switch (aa_getevent(ctx, 0)) {
			case AA_RESIZE:
				fprintf(stderr, "sizes before: %dx%d, %dx%d\n", aa_scrwidth(ctx), aa_scrheight(ctx), aa_imgwidth(ctx), aa_imgheight(ctx));
				//aa_resize(ctx); // this doesn't work for some reason, so we recreate the context...
				aa_close(ctx);
				ctx = init_aa_ctx();
				fprintf(stderr, "sizes after: %dx%d, %dx%d\n", aa_scrwidth(ctx), aa_scrheight(ctx), aa_imgwidth(ctx), aa_imgheight(ctx));
				break;
			case AA_ESC:
				aa_close(ctx);
				return;
				break;
		}
		ripple_count = ripples_tick(ripples, ripple_count, max_ripples, tick_interval, aa_imgwidth(ctx), aa_imgheight(ctx));
		draw_ripples(ctx, ripples, ripple_count);
		sync_vbuf(ctx, &next_tick);
		TIME_ADD_NS(next_tick, tick_ns);
	}
}

int main(int argc, char *argv[]) {
	run_loop();
	return 0;
}