# Ripples

Draws some nice ripples in your terminal. Hypnotic.

## Building
Install `aalib` (available in most distributions) and run make.

## Compatibility
Tested on MacOS and Linux.

## License
Released under the 0-Clause BSD license.
